<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class CheckController extends Controller
{
    public function check(Request $request) {

        $user= $request->user();
        if($user['role']=='user'){
            return response('this is user',200);
        }
        
        return $request->user();
    }
}
