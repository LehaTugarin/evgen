<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Issue;
use App\Models\User_create;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class IssueController extends Controller
{
    private function getIssuesForUser($user){

        $closedIssues = DB::table('issue')
        ->join('user_create','issue.id_issue','=','user_create.id_issue')
        ->join('issue_status','issue.id_status','=','issue_status.id_status')
        ->where('user_create.id_user',$user['id'])
        ->where('issue.id_status',5)
        ->orderBy('issue.id_issue','desc')
        ->get();

        $openedIssues = DB::table('issue')
        ->join('user_create','issue.id_issue','=','user_create.id_issue')
        ->join('issue_status','issue.id_status','=','issue_status.id_status')
        ->where('user_create.id_user',$user['id'])
        ->where('issue.id_status','!=',5) //!=5 
        ->orderBy('issue.id_issue','desc')
        ->get();
        
        return ['opened'=>$openedIssues,'closed'=>$closedIssues];
    }
    //categories
    public function categories(Request $request) {

        $categories = Category::all();

        $result =[];

        foreach($categories as $ct){
            array_push($result,
            array(
                'value' => $ct->id_category,
                'text' => $ct->name_category
            ));
        }

        return $result;
    }

    public function create(Request $request){
        $issue = Issue::create([
            'id_category' => $request['id_category'], 
            'title' => $request['title'], 
            'description' => $request['description'], 
            'priority' => $request['priority'], 
            'create_date' => now(),
            'id_status' => '1'
        ]);
        
        $user = $request->user();

        User_create::create([
            'id_issue' => $issue['id_issue'],
            'id_user' => $user['id']
        ]);

        $res = $this->getIssuesForUser($user);
        return $res;
    }

    public function my_issues(Request $request){
        $user = $request->user();
        $res = $this->getIssuesForUser($user);
        return $res;
    }
}
