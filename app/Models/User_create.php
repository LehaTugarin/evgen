<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_issue
 * @property int $id_user
 * @property User $user
 * @property Issue $issue
 */
class User_create extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_create';

    /**
     * @var array
     */
    protected $fillable = ['id_issue','id_user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issue()
    {
        return $this->belongsTo('App\Issue', 'id_issue', 'id_issue');
    }
}
