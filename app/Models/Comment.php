<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_comment
 * @property int $id_issue
 * @property string $text
 * @property string $created_at
 * @property boolean $readet
 * @property Issue $issue
 */
class Comment extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'comment';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_comment';

    /**
     * @var array
     */
    protected $fillable = ['id_issue', 'text', 'created_at', 'readet'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issue()
    {
        return $this->belongsTo('App\Issue', 'id_issue', 'id_issue');
    }
}
