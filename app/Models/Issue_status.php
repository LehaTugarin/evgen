<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_status
 * @property int $id_issue
 * @property int $id_history
 * @property string $name_status
 * @property Issue $issue
 * @property IssueHistory $issueHistory
 */
class Issue_status extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'issue_status';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_status';

    /**
     * @var array
     */
    protected $fillable = ['id_issue', 'id_history', 'name_status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issue()
    {
        return $this->belongsTo('App\Issue', 'id_issue', 'id_issue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issueHistory()
    {
        return $this->belongsTo('App\IssueHistory', 'id_history', 'id_history');
    }
}
