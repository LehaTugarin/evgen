<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_issue
 * @property int $id_category
 * @property string $title
 * @property string $description
 * @property string $priority
 * @property string $create_date
 * @property string $resolve_date
 * @property Category $category
 * @property IssueStatus[] $issueStatuses
 * @property IssueHistory[] $issueHistories
 * @property User[] $users
 * @property Comment[] $comments
 */
class Issue extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'issue';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_issue';

    /**
     * @var array
     */
    protected $fillable = ['id_category', 'title', 'description', 'priority', 'create_date', 'resolve_date','id_status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category', 'id_category', 'id_category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issueStatuses()
    {
        return $this->hasMany('App\IssueStatus', 'id_issue', 'id_issue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issueHistories()
    {
        return $this->hasMany('App\IssueHistory', 'id_issue', 'id_issue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_create', 'id_issue', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'id_issue', 'id_issue');
    }
}
