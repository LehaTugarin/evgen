<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_group
 * @property string $name_group
 * @property string $description
 * @property User[] $users
 */
class Tech_group extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tech_group';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_group';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['name_group', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'id_group', 'id_group');
    }
}
