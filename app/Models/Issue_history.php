<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_history
 * @property int $id_issue
 * @property int $id_user
 * @property string $event_date
 * @property User $user
 * @property Issue $issue
 * @property IssueStatus[] $issueStatuses
 */
class Issue_history extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'issue_history';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_history';

    /**
     * @var array
     */
    protected $fillable = ['id_issue', 'id_user', 'event_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function issue()
    {
        return $this->belongsTo('App\Issue', 'id_issue', 'id_issue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issueStatuses()
    {
        return $this->hasMany('App\IssueStatus', 'id_history', 'id_history');
    }
}
