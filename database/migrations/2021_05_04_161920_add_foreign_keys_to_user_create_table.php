<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_create', function (Blueprint $table) {
            $table->foreign('id_user', 'fk_user_cre_create2_user')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_issue', 'fk_user_cre_create_issue')->references('id_issue')->on('issue')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_create', function (Blueprint $table) {
            $table->dropForeign('fk_user_cre_create2_user');
            $table->dropForeign('fk_user_cre_create_issue');
        });
    }
}
