<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToIssueHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issue_history', function (Blueprint $table) {
            $table->foreign('id_user', 'fk_issue_hi_activate_user')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_issue', 'fk_issue_hi_has_issue')->references('id_issue')->on('issue')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issue_history', function (Blueprint $table) {
            $table->dropForeign('fk_issue_hi_activate_user');
            $table->dropForeign('fk_issue_hi_has_issue');
        });
    }
}
