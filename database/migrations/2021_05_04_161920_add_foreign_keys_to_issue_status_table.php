<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToIssueStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issue_status', function (Blueprint $table) {
            $table->foreign('id_issue', 'fk_issue_st_has2_issue')->references('id_issue')->on('issue')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_history', 'fk_issue_st_include_issue_hi')->references('id_history')->on('issue_history')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issue_status', function (Blueprint $table) {
            $table->dropForeign('fk_issue_st_has2_issue');
            $table->dropForeign('fk_issue_st_include_issue_hi');
        });
    }
}
