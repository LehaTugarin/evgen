import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginComponent from "../components/Login.vue"
import AppComponent from "../components/App.vue"
import UserCabinet from "../components/UserCabinet/Cabinet"
//import SecureComponent from "../views/secure.vue"

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/',
            component:AppComponent
            // redirect: {
            //     name: "login"
            // }
        },
        {
            path: "/login",
            name: "login",
            component: LoginComponent
        },
        {
            path: "/user-cabinet",
            name: "user-cabinet",
            component: UserCabinet
        }
    ]
})