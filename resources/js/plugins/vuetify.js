import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/lib/util/colors'
Vue.use(Vuetify)

const opts = {

    theme: {
        themes: {
            light: {
                primary: colors.green.darken1, // #E53935
                secondary: colors.blue.lighten1, // #FFCDD2
                accent: colors.indigo.base, // #3F51B5
            },
        },
        dark: false
    },
}

export default new Vuetify(opts)