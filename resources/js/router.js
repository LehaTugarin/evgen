import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './components/Login.vue'

Vue.use(VueRouter);

const routes = [
    { path: '/login', component: Login },
    //{ path: '/bar', component: Bar }
]


export default VueRouter({
    routes // short for `routes: routes`
})