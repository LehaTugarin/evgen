
require('./bootstrap');

window.Vue = require('vue').default;

Vue.component("app-component", require("./components/App.vue").default);


import store from './store';
import router from './route';

import 'material-design-icons-iconfont/dist/material-design-icons.css';
import vuetify from './plugins/vuetify';
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

const app = new Vue({
    el: '#app',
    store,
    router,
    vuetify
    
});
