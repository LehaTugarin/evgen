
import router from "../../route";
export default {
    state: {
        issueErrors: null,
        issueList: []
    },
    actions: {
        ajaxCreateIssue(context, bodyParams) {
            axios.post('/api/create-issue', bodyParams)
                .then(response => {
                    console.log(response.data);
                    context.commit('setIssueList', response.data);

                }).catch(error => {
                    console.log(error);
                    if (error.response.status == 401) {
                        context.commit('setIssueErrors', 'Требуется авторизация');
                        localStorage.clear();
                        router.push("login");
                    } else {
                        context.commit('setIssueErrors', 'Проверьте правильность заполнения формы');
                    }
                });
        },
        ajaxLoadIssueList(context, bodyParams) {
            axios.post('/api/my-issue', bodyParams)
                .then(response => {
                    console.log(response.data);
                    context.commit('setIssueList', response.data);

                }).catch(error => {
                    console.log(error);
                    if (error.response.status == 401) {
                        context.commit('setIssueErrors', 'Требуется авторизация');
                        localStorage.clear();
                        router.push("login");
                    } else {
                        context.commit('setIssueErrors', 'Проверьте правильность заполнения формы');
                    }
                });
        }
    },
    mutations: {
        setIssueErrors(state, data) {
            state.issueErrors = data
        },
        setIssueList(state, data) {
            state.issueList = data
        }
    },
    getters: {
        getIssueList(state) {
            return state.issueList
        },
        getIssueErrors(state) {
            return state.issueErrors
        }
    }
}