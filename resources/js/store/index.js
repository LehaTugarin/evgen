
import Vue from 'vue';
import Vuex from 'vuex';

import router from "../route";

import issue from "./modules/issue";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: JSON.parse(localStorage.getItem('user')),
        loginErrors: null,
        issueCategories: null
    },
    getters: {
        getUser(state) {
            return state.user
        },
        getLoginErrors(state) {
            return state.loginErrors
        },
        getIssueCategories(state) {
            return state.issueCategories
        }
    },
    actions: {
        //logout
        logout(context){
            context.commit('setUser', null);
            axios.defaults.headers.common = { 'Authorization': `Bearer ${null}` }
            localStorage.clear();
            router.push("login");
        },
        // load user from local Storare
        setUser(context, data) {
            context.commit('setUser', data);
            context.commit('setLoginErrors', null);
            axios.defaults.headers.common = { 'Authorization': `Bearer ${data.token}` }
            localStorage.setItem('user', JSON.stringify(data));
            //setTimeout(()=>router.push("user-cabinet"),10000);
        },
        // login action
        ajaxLoginAction(context, params) {
            axios.post('/api/login', params)
                .then(response => {
                    console.log(response.data);
                    context.commit('setUser', response.data);
                    context.commit('setLoginErrors', null);
                    axios.defaults.headers.common = { 'Authorization': `Bearer ${response.data.token}` }
                    localStorage.setItem('user', JSON.stringify(response.data));
                    router.push("user-cabinet");
                }).catch(error => {
                    console.log(error);
                    if (error.response.status == 401) {
                        context.commit('setLoginErrors', 'Неверный логин или пароль');
                    } else {
                        context.commit('setLoginErrors', 'Неполадки на сервере');
                    }
                });
        },
        resetLoginError(context) {
            context.commit('setLoginErrors', null)
        },
        // categories action
        ajaxLoadCategoriesAction(context, params) {
            axios.get('/api/categories')
                .then(response => {
                    console.log(response.data);
                    context.commit('setCategories', response.data);

                }).catch(error => {
                    console.log(error);
                    // if (error.response.status == 401) {
                    //     context.commit('setLoginErrors', 'Неверный логин или пароль');
                    // } else {
                    //     context.commit('setLoginErrors', 'Неполадки на сервере');
                    // }
                });
        },
    },
    mutations: {
        setUser(state, data) {
            state.user = data
        },
        setLoginErrors(state, data) {
            state.loginErrors = data
        },
        setCategories(state, data) {
            state.issueCategories = data
        }
    },
    modules: {
        issue,
    }
});